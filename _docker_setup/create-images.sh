#!/usr/bin/env bash


eval `docker-machine env manager1`

array=(
    './payment-service'
    './notification-service'
)

# We go to the root of the project 
cd ..

for ((i = 0; i < ${array[@]}; ++i)) ; do

#Go to the each folder
cd ${array[$i]}

# We get the name of our image
SERVICE=$(echo $array[$i] |cut -d'/' -f 2)

# We delete the image if ti exists already
docker rmi valentinlucas/$SERVICE

# We create or recreate our image
sh ./create-image.sh

# We get the image id so we can tag it 
IMAGE_ID=$(docker image -q $SERVICE)

# We tag our image so we can publish it to our docker
docker push valentinlucas/$SERVICE:latest

# We delete our local image because we are not going to need it 
# and maintain clean our environment
docker rmi $SERVICE

# And we go back to the root again 

cd ..
done