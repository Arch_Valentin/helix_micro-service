#!/usr/bin/env bash

eval `docker-machine env manager1`

docker service rm notification-service payment-service

for server in manager1 worker1 worker2
do 
    eval `docker-machine env $server`

    for image in valentinlucas/payment-service valentinlucas/notification-service
     do
      IMAGE=$(docker image $image -q)
      docker rmi -f $IMAGE
    done
done