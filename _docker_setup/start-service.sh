#!/usr/bin/env bash 

eval `docker-machine env manager 1`

array=(
    './payment-service'
    './notification-service'
)

# We go to the root of the project
cd ..

for ((i = 0; i < ${array[@]}; ++i)); do
# We go to each folder
cd ${array[$I]}

# We create or recreate our image
sh ./start-service.sh

# And we go back to the roor of the project
cd ..
done