'use strict'

const repository = (container) => {
    const sendEmail = (payload) => {
        return new Promise((resolve, reject) => {
            const {smtpSettings, smtpTransport, nodemailer} = container.cradle;


            const transporter = nodemailer.createTransport(
                smtpTransport({
                    service: smtpSettings.service,
                    auth:{
                        user: smtpSettings.user,
                        pass: smtpSettings.pass
                    }
                }));

                const mailOptions = {
                    from: '"Do not reply, Helix Company 👥"<no-replay@helix.com',
                    to: `${payload.user.email}`,
                    subject: 'Test',
                    html: `
                        <h1>Description: ${payload.description}</h1
                    `
                };

                transporter.sendEmail(mailOptions, (err, info) => {
                    if(err) reject(new Error('An error occured sending an email, err: ' + err));
                    transporter.close();
                    resolve(info);
                });
        })
    }

    const sendSMS = (payload) => {
        // TODO : Maybe orange api or obs 
    }

    return Object.create({
        sendSMS,
         sendEmail
    });
};

const connect = (container) => {
    return new Promise((resolve, reject) => {
        if(!container) reject(new Error('dependencies not supplied'));
        resolve(repository(container));
    });
}

module.exports = Object.assign({}, {container});