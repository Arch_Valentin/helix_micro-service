const notificationSchema = (joi) => ({
  city: joi.string(),
  orderId: joi.string(),
  _id: joi.string(),
  description: joi.string(),
  user: joi.object().keys({
    name: joi.string(),
    email: joi.string().email()
  })
});

module.exports = notificationSchema;