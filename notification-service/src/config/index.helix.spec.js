/* eslint-env mocha */
const {EventEmitter} = require('events');
const test = require('assert');
const {init} = require('./');

describe('Helix configuration', () => {
    it('can init dependencies to the container', (done) =>{
        const events = new EventEmitter();

        events.on('helix.ready', (container) => {
            console.log(database);
            console.log(container);
            done();
        });

        helix.on('helix.error', err => {
            console.log(err);
            done();
        });


        init(events);

        events.emit('init');
    });
});