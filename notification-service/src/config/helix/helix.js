const { createContainer, asValue } = require('awilix')
const nodemailer = require('nodemailer')
const smtpTransport = require('nodemailer-smtp-transport')

function initHelix ({serverSettings, models, smtpSettings}, events) {
  events.once('init', () => {
    const container = createContainer()

    container.register({
      validate: asValue(models.validate),
      serverSettings: asValue(serverSettings),
      smtpSettings: asValue(smtpSettings),
      nodemailer: asValue(nodemailer),
      smtpTransport: asValue(smtpTransport)
    })

    events.emit('di.ready', container)
  });
};

module.exports.initHelix = initHelix;
