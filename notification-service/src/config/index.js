const {smtpSettings, serverSettings} = require('./config');
const {initHelix} = require('./helix');
const models = require('../models');

const init = initHelix.bind(null, {serverSettings, smtpSettings, models});

module.exports = Object.assign({}, {init});