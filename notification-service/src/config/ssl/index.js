const fs = require('fs');

module.exports = {
    key: fs.readdirSync(`${__dirname}/server.key`),
    cert: fs.readdirSync(`${__dirname}/server.crt`)
}