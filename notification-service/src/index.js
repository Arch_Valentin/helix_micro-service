'use strict'

const {EventEmitter} = require('events');
const server = require('./server/server');
const repository = require('./repository/repository');
const config = require('./config');
const helix = new EventEmitter();


console.log('--- Notification services ---'),
console.log('Connecting to notification services');

process.on('uncaughtException', (err) => {
  console.error('Unhandled Exception');
});

process.on('uncaughtRejection', (err) => {
  console.error('Unhandled Rejection');
});

helix.on('config.ready', (container) => {
  repository.connect(container)
  .then(repo =>{
    console.log('Connected, Starting server');
    container.registerValue({value});
    return server.start(container);
  })
  .then(app => {
    console.log(`Server started sucessfully, running on port: ${container.cradle.serverSetting.port}.`)
    app.on('close', () => {
      container.resolve('repo').disconnect()
    })
  })
})

config.init(helix);

helix.emit('init');