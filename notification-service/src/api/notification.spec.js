/* eslint-env mocha */

const {createContainer, asValue} = require('awilix');
const {smtpSettings} = require('../config/config');
const nodemailer = require('nodemailer'),
smtpTransport = require('nodemailer-smtp-transport'),
should = require('should'),
request = require('supertest'),
server = require('../server/server'),
models = require('../models');

