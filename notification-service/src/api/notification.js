'use strict'


const status = require('http-status');

module.export = ({repo}, app => {
    app.post('/notification/sendEmail', (request, response, next) => {
        const {validate} = request.container.cradle;

        validate(request.body.payload, 'notification')
        .then(payload => {
            return repo.sendEmail(payload);
        })
        .then(ok => {
            response.status(status.OK).json({msg: 'OK'});
        })
        .catch(next);
    });


    app.post('/notification/sendSMS', (request, response, next) => {
        const {validate} = request.container.cradle;

        validate(request.body.payload, 'notification')
        .then(payload => {
            return repo.sendSMS(payload);
        })
        .then(ok => {
            response.status(status.OK).json({msg: 'OK'});
        })
        .catch(next);
    });

})