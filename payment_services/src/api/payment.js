'use strict'

const status = require('http-status');

module.exports = ({repo}, app) => {
    app.post('/payment/makePurchase', (request, response, next) => {
        const {validate} = request.container.cradle;

        validate(request.body.paymentOrder, 'payment')
        .then(payment => {
            return repo.registerPurchase(payment);
        })
        .then(paid => {
            response.status(status.OK).json({paid});
        })
        .catch(next);
    });
} 

app.get('/payment/getPurchaseById/:id', (request, response, next) => {
    repo.getPurchaseById(request.params.id)
    .then(payment => {
        response.status(status.OK).json({payment});
    })
    .catch(next);
});