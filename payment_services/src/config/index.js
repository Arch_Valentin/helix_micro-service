const {dbSetting, serverSettings, stripeSettings} = require('./config'),
database = require('./db'),
{initHelix} = require('./helix'),
models = require('../models');

const init = initHelix.bind(null, {serverSettings, dbSettings, database, models, stripeSettings});

module.exports = Object.assign({}, {init});
