const MongoClient = require('mongodb');

const getMongoURL = (options) => {
  const url = options.SERVERS
  .reduce((prev, cur) => prev + cur + ',', 'mongodb://');

  return `${url.substr(0, url.length - 1)}/${options.db}`
}

const connect = (options, events) => {
  events.once('boot.ready', () => {
    MongoClient.connect(
      getMongoURL(options), {
        db: options.dbParameters(),
        server: options.serverParameters(),
        replset: options.replsetParameters(options.repl)
      }, (err, db) => {
        if(err){
          events.emit('db.error', err);
        }

        db.admin().authenticate(options.user, options.pass, (err, result) => {
          if(err) events.emit('db.error', err);
        
        events.emit('db.ready', db);
      });
    });
  });
};

module.exports = Object.assign({}, {connect});
