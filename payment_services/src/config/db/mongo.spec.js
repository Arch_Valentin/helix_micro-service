/* eslint-env mocha */
const {EventEmitter} = require('events')
const test = require('assert')
const mongo = require('./mongo')
const {dbSettings} = require('./config')

describe('Mongo Connection', () => {
  it('should emit db Object with an EventEmitter', (done) => {
    const events = new EventEmitter()

    events.on('db.ready', (db) => {
      db.admin().listDatabases((err, dbs) => {
        test.equal(null, err)
        test.ok(dbs.databases.length > 0)
        console.log(dbs.databases)
        db.close()
        done()
      })
    })

    events.on('db.error', (err) => {
      console.log(err)
    })

    mongo.connect(dbSettings, events)

    events.emit('boot.ready')
  })
})
