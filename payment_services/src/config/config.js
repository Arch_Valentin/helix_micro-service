const dbSetings = {
  db: process.env.DB || 'payment',
  user: process.env.DB_USER || 'valentin',
  pass: process.env.DB_pASS || 'valentin0987654321',
  repl: process.env.DB_REPLS || 'rs1',
  servers: (process.env.DB-SERVERS) ? process.env.DB_SERVERS.substr(1, process.env.DB_SERVERS.lengh - 2).split(' ') : [
    'localhost'
  ],
  dbParameters: () => ({
    w: 'majority',
    wtimeout: 10000,
    j: true,
    readPreference: 'ReadPrefence.SECONDARY_PREFERRED',
    native_parser: false
  }),
  serverParameters: () =>({
    autoReconnect: true,
    poolSize: 10,
    socketoptions: {
      keepALive: 300,
      connectTimeoutMS: 30000,
      socketTimeoutMS: 30000
    }
  }),
  replsetParameters: (replset = 'rs1') => ({
    replicatSet: replset,
    ha: true,
    haInterval: 10000,
    poolSize: 10,
    socketoptions:{
      keepAlive: 300,
      connectTimeoutMS: 30000,
      socketTimeoutMS: 30000
    }
  })
};


const serverSettings = {
    port: process.env.PORT || 3000,
    ssl: require('./ssl')
};

const stripeSettings = {
    secret: 'sk_test_lPPoJjmmbSjymtgo4r0O3z89',
    public: 'pk_test_l10342hIODZmOJsBpY6GVPHj'
};



module.exports = Object.assign({}, {dbSettings, serverSettings, stripeSettings});
