const { createContainer, asValue } = require('awilix')
const stripe = require('stripe')

function inithelix ({serverSettings, dbSettings, database, models, stripeSettings}, events) {
  events.once('init', () => {
    events.on('db.ready', (db) => {
      const container = createContainer()

      container.register({
        database: asValue(db),
        validate: asValue(models.validate),
        ObjectID: asValue(database.ObjectID),
        serverSettings: asValue(serverSettings),
        stripe: asValue(stripe(stripeSettings.secret))
      })

      events.emit('di.ready', container)
    });

    events.on('db.error', (err) => {
      events.emit('di.error', err)
    });

    database.connect(dbSettings, events);

    events.emit('boot.ready');
  });
};

module.exports.inithelix = inithelix